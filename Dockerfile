FROM quay.io/astronomer/ap-airflow:2.0.0-3-buster-onbuild

USER root
RUN pip uninstall -y apache-airflow-providers-google apache-airflow-providers-microsoft-azure
USER astro