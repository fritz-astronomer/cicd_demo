# CI/CD Demo
General CI/CD Steps
- Provision test environment w/ python, download astro
- Test dags with `python`
- Run flake8
- Run pytest
- Build with docker
- Run astro deploy

## Github Actions
https://docs.github.com/en/actions/quickstart

## Gitlab
https://docs.gitlab.com/ee/ci/README.html

## CircleCI
https://circleci.com/docs/2.0/hello-world/?section=getting-started

## Amazon CodePipeline

## Jenkins

## TravisCI